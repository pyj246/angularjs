var IndexAppModule = angular.module('indexApp', []);

IndexAppModule.constant('appConstants', {
	'appTitle':'Application Demo'
});

IndexAppModule.controller('IndexController', function ($scope, appConstants) {
	var index = this;
	$scope.appConstants = appConstants;
	
	index.apps = [
	    {title:'MOBILE'},
		{title:'TODO'},
		{title:'PROJECT'},
		{title:'COMPONENTS'},
		{title:'STORE'}
	]
	
	index.goPage = function (app) {
		var path = app['path'];
		if (typeof path !== 'string' || path.length < 1) {
			var titleLowercase = angular.lowercase(app['title']);
			path = '/appHtml/'.concat(
				titleLowercase, '/', titleLowercase, '.html'
			);
		}
		location.href = path;
	}
});