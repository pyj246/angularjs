var todoAppModule = angular.module('todoApp', []);

todoAppModule.constant('appConstants', {
	'appTitle':'Todo'
});

todoAppModule.controller('TodoListController', function ($scope, appConstants) {
	var todoList = this;
	$scope.appConstants = appConstants;
	
	todoList.todos = [
  	 	{done:true, 	title:'AngularJS 독서'},
	 	{done:false,	title:'AngualrJS 공부하기'},
	 	{done:false, 	title:'개인프로젝트 구성'}
	];
	
	todoList.addTodo = function () {
		todoList.todos.push({
			title:todoList.todoTitle, done:false
		});
	};
	
	todoList.remaining = function () {
		var count = 0;
		angular.forEach(todoList.todos, function (todo) {
			count += todo.done? 0:1;
		});
		return count;
	};
	
	todoList.archive = function () {
		var oldTodos = todoList.todos;
		todoList.todos = [];
		angular.forEach(oldTodos, function (todo) {
			if (!todo.done) {
				todoList.todos.push(todo);
			}
		});
	};
});