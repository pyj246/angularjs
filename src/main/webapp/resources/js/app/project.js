var projectAppModule = angular.module('projectApp', ['ngRoute', 'firebase']);

// constant
projectAppModule
	.constant('appConstants', {
		'appTitle': 'Project'
	});

// value
projectAppModule
	.value('fbURL', 'https://ng-projects-list.firebaseio.com/');

// service
projectAppModule
	.service('fbRef', function (fbURL) {
		return new Firebase(fbURL);
	})
	.service('fbAuth', function ($q, $firebase, $firebaseAuth, fbRef) {
		var auth;
		return function () {
			if (auth) 
				return $q.when(auth);
			
			var authObj = $firebaseAuth(fbRef);
			if (authObj.$getAuth())
				return $q.when(auth = authObj.$getAuth());
			
			var deferred = $q.defer();
			authObj.$authAnonymously().then(function (authData) {
				auth = authData;
				deferred.resolve(authData);
			});
			return deferred.promise;
		};
	}).
	service('Projects', function ($q, $firebase, fbRef, fbAuth) {
		var self = this;
		this.fetch = function () {
			if (this.projects)
				return $q.when(this.projects);
			
			return fbAuth().then(function (auth) {
				var deferred = $q.defer(),
					ref = fbRef.child('projects-fresh/' + auth.auth.uid),
					$projects = $firebase(ref);
				ref.on('value', function (snapshot) {
					var projectsArray = window.projectsArray;
					if (projectsArray === undefined)
						projectsArray = [];
					
					if (snapshot.val() === null)
						$projects.$set(projectsArray);
					
					self.projects = $projects.$asArray();
					deferred.resolve(self.projects);
				});
				
				ref.onDisconnect().remove();
				return deferred.promise;
			});
		}
	});


// config
projectAppModule
	.config(function ($routeProvider) {
		var resolveProjects = {
			projects: function (Projects) {
				return Projects.fetch();
			}	
		}
		
		$routeProvider
			.when('/', {
				controller:'ProjectListController as projectList',
				templateUrl:'list.html',
				resolve:resolveProjects
			})
			.when('/new', {
				controller:'ProjectCreateController as projectEdit',
				templateUrl:'detail.html',
				resolve:resolveProjects
			})
			.when('/edit/:projectId', {
				controller:'ProjectUpdateController as projectEdit',
				templateUrl:'detail.html',
				resolve:resolveProjects
			})
			.otherwise({
				redirectTo:'/'
			});
	});


//controller
projectAppModule
	.controller('ProjectController', function ($scope, appConstants) {
		$scope.appConstants = appConstants;
	})
	.controller('ProjectListController', function (projects) {
		var projectList = this;
//		$scope.appConstants = appConstants;
		
		projectList.projects = projects
	})
	.controller('ProjectCreateController', function ($location, projects) {
		var projectEdit = this;
		projectEdit.save = function () {
			projects.$add(projectEdit.project).then(function (data) {
				$location.path('/');
			});
		}
	})
	.controller('ProjectUpdateController', function ($location, $routeParams, projects) {
		var projectEdit = this,
			projectId = $routeParams.projectId,
			projectIndex;
		projectEdit.projects = projects;
		projectIndex = projectEdit.projects.$indexFor(projectId);
		projectEdit.project = projectEdit.projects[projectIndex];
		
		projectEdit.destory = function () {
			projectEdit.projects.$remove(projectEdit.project).then(function (data) {
				$location.path('/');
			});
		}
		
		projectEdit.save = function () {
			projectEdit.projects.$save(projectEdit.project).then(function (data) {
				$location.path('/');
			})
		}
	});